import 'package:landrope/helper/constant.dart';
import 'package:landrope/helper/routes.dart';
import 'package:landrope/helper/style.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Landrope 2.0',
      initialRoute: Paths.assign,
      onGenerateRoute: Routers.generateRoute,
      theme: ThemeData(
        textTheme: GoogleFonts.poppinsTextTheme(Theme.of(context).textTheme),
        primaryColor: Style.primaryColor,
        accentColor: Style.primaryColor,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
    );
  }
}
