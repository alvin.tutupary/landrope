import 'package:landrope/helper/constant.dart';
import 'package:landrope/model/assign_details.dart';
import 'package:landrope/model/assigns.dart';
import 'package:landrope/model/mobile_tugas_keydt.dart';
import 'package:landrope/ui/assign/assign_page.dart';
import 'package:landrope/ui/assign_detail/assign_detail_page.dart';
import 'package:landrope/ui/assign_detail/assign_detail_submit.dart';
import 'package:landrope/ui/login/login_page.dart';
import 'package:flutter/material.dart';

class Routers {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case Paths.assign:
        return MaterialPageRoute(builder: (_) => AssignPage());

      case Paths.assign_detail:
        var data = settings.arguments as AssignRows;
        return MaterialPageRoute(builder: (_) => AssignDetailPage(data: data));

      case Paths.assign_detail_submit:
        var data = settings.arguments as AssignDetailSubmitArgs;
        return MaterialPageRoute(
            builder: (_) => AssignDetailSubmitPage(data: data));

      case Paths.login:
        return MaterialPageRoute(builder: (_) => LoginPage());

      default:
        return MaterialPageRoute(builder: (_) {
          return Scaffold(
            body: Center(
              child: Text('No route defined for ${settings.name}'),
            ),
          );
        });
    }
  }
}

class AssignDetailSubmitArgs {
  final AssignRows dataAssignRows;
  final AssignDetailRows dataAssignDetailRows;
  final MobileTugasKeydt tugasKeyDt;

  AssignDetailSubmitArgs(
      {this.dataAssignRows, this.dataAssignDetailRows, this.tugasKeyDt});
}
