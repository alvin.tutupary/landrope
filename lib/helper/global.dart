import 'package:intl/intl.dart';

class Global {
  factory Global() => _instance;

  String mobileSelectedKey;
  String username;

  bool isBussy;
  bool isLogout;

  static final Global _instance = Global._internal();

  Global._internal() {
    isBussy = false;
  }

  String dateFormat(String date) {
    if (date != null) {
      DateTime dates = DateTime.parse(date);
      DateFormat pattern = DateFormat('dd MMM yy h:mm a');
      String dateString = pattern.format(dates);
      return dateString;
    } else {
      return "-";
    }
  }

  String strStep(int step) {
    switch (step) {
      case 1:
        return "Akta Notaris";
      case 2:
        return "PBT Perorangan";
      case 3:
        return "SPH";
      case 4:
        return "PBT PT";
      case 5:
        return "SK BPN";
      case 6:
        return "Cetak Buku";
      case 7:
        return "AJB";
      case 8:
        return "AJB Hibah";
      case 9:
        return "SHM Hibah";
      case 10:
        return "Penurunan Hak";
      case 11:
        return "Peningkatan Hak";
      case 12:
        return "Balik Nama";
      case 13:
        return "Pengukuran";
      case 14:
        return "Riwayat Tanah";
      case 15:
        return "Bayar PPh";
      case 16:
        return "Validas _PPh";
      case 17:
        return "Bayar BPHTB";
      case 18:
        return "Validasi BPHTB";
      default:
        return "Uncategories";
    }
  }

  String strCategories(int categoriy) {
    switch (categoriy) {
      case 1:
        return "Girik";
      case 2:
        return "HGB";
      case 3:
        return "SHM";
      case 4:
        return "SHP";
      case 5:
        return "Hibah";
      default:
        return "Unknown";
    }
  }
}

final globalData = Global();
