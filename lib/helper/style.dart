import 'package:flutter/material.dart';

class Style {
  static const bgColor = Color(0xFFF1EFF1);
  static const primaryColor = Color(0xFF2A166D);
  static const secondaryColor = Color(0xFFFFA41B);
  static const textColor = Color(0xFF000839);
  static const textLightColor = Color(0xFF747474);
  static const blueColor = Color(0xFF40BAD5);
  static const greenColor = Color(0xFF2CC675);

  static const defaultShadow = BoxShadow(
    offset: Offset(0, 15),
    blurRadius: 27,
    color: Colors.black12, // Black color with 12% opacity
  );

  static const defaultPadding = 20.0;

  static final baseTextStyle = const TextStyle(fontFamily: 'Roboto');
  static final smallTextStyle = commonTextStyle.copyWith(
    fontSize: 9.0,
  );
  static final commonTextStyle = baseTextStyle.copyWith(
      color: const Color(0xffb6b2df),
      fontSize: 14.0,
      fontWeight: FontWeight.w400);
  static final titleTextStyle = baseTextStyle.copyWith(
      color: Colors.white, fontSize: 30.0, fontWeight: FontWeight.w600);
  static final subTitleTextStyle = baseTextStyle.copyWith(
      color: Colors.white, fontSize: 25.0, fontWeight: FontWeight.w600);
  static final headerTextStyle = baseTextStyle.copyWith(
      color: Colors.white, fontSize: 15.0, fontWeight: FontWeight.w400);
  static final subHeaderTextStyle = baseTextStyle.copyWith(
      color: Colors.white, fontSize: 15.0, fontWeight: FontWeight.w400);
}
