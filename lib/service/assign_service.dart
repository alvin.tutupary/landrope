import 'package:landrope/helper/app_settings.dart';
import 'package:landrope/model/assign_details.dart';
import 'package:landrope/model/assigns.dart';
import 'package:dio/dio.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

class AssignService {
  Dio api = new Dio();
  final _storage = new FlutterSecureStorage();

  Future<Assigns> getAssignLisit() async {
    try {
      final token = await _storage.read(key: "token");
      Assigns resData = new Assigns();
      final result = await api
          .get('${app.baseAddress}assign/list?token=$token&pg=1&rpp=100');
      if (result.statusCode == 200) {
        resData = Assigns.fromJson(result.data);
      }
      return resData;
    } catch (e) {
      throw e;
    }
  }

  Future<AssignDetails> getAssignDtlLisit(String assignKey) async {
    try {
      final token = await _storage.read(key: "token");
      AssignDetails resData = new AssignDetails();
      final result = await api.get(
          '${app.baseAddress}assign/dtl/list?token=$token&asgnkey=$assignKey&pg=1&rpp=100');
      if (result.statusCode == 200) {
        resData = AssignDetails.fromJson(result.data);
      }
      return resData;
    } catch (e) {
      throw e;
    }
  }
}
