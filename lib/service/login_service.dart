import 'package:landrope/helper/app_settings.dart';
import 'package:dio/dio.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

class LoginService {
  Dio auth = new Dio();
  final _storage = new FlutterSecureStorage();

  Future doLogin(String username, String password) async {
    try {
      Response res = await auth.post(app.baseAuth + "login",
          data: {"username": username, "password": password, "mobile": true});
      await _storage.write(key: "token", value: res.data);
    } catch (e) {
      throw e;
    }
  }

  Future doLogout() async {
    try {
      await _storage.deleteAll();
    } catch (e) {
      throw e;
    }
  }
}
