import 'package:landrope/helper/app_settings.dart';
import 'package:landrope/helper/global.dart';
import 'package:landrope/model/assign.dart';
import 'package:landrope/model/mobile_tugas_keydt.dart';
import 'package:dio/dio.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

class MobileService {
  Dio api = new Dio();
  final _storage = new FlutterSecureStorage();
  Future<Assign> getAssign(String key) async {
    try {
      final token = await _storage.read(key: "token");
      Assign resultDecode = new Assign();

      final result =
          await api.get('${app.baseAddress}mobile/get?token=$token&key=$key');
      if (result.statusCode == 200) {
        resultDecode = Assign.fromJson(result.data);
      }
      return resultDecode;
    } catch (e) {
      throw e;
    }
  }

  Future<bool> getMobileAllowed(String username) async {
    try {
      final result =
          await api.get('${app.baseAddress}mobile/allowed?username=$username');
      if (result.data) {
        globalData.username = username;
      }
      return result.data;
    } catch (e) {
      throw e;
    }
  }

  Future postTugasStep(String assignKey, String rootKey) async {
    try {
      final token = await _storage.read(key: "token");
      Response res = await api.post(
          app.baseAddress + "mobile/tugas/step?token=$token",
          data: {"akey": assignKey, "rkey": rootKey, "control": 0});
      return res;
    } catch (e) {
      throw e;
    }
  }

  Future<MobileTugasKeydt> postTugasKeydt() async {
    try {
      int category = 1, step = 1;
      await _storage.read(key: "token");
      MobileTugasKeydt resultDecode = new MobileTugasKeydt();
      Response res = await api
          .post(app.baseAddress + "mobile/tugas/keydt/$category/$step");
      if (res.statusCode == 200) {
        resultDecode = MobileTugasKeydt.fromJson(res.data);
      }
      return resultDecode;
    } catch (e) {
      throw e;
    }
  }

  Future postTugasDtlSubmit(String keyDT, int mkey, String propVal, String dKey,
      String rKey, String aKey) async {
    try {
      final token = await _storage.read(key: "token");
      Response res = await api.post(
          app.baseAddress + "mobile/tugas/dtl/submit?token=$token",
          data: {
            "info": {
              "keyDT": keyDT,
              "props": [
                {"mkey": mkey, "val": propVal}
              ]
            },
            "dkey": dKey,
            "akey": aKey,
            "rkey": rKey,
            "control": 0
          });
      return res;
    } catch (e) {
      throw e;
    }
  }
}
