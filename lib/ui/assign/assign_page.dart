import 'package:landrope/helper/constant.dart';
import 'package:landrope/helper/style.dart';
import 'package:landrope/service/login_service.dart';
import 'package:flutter/material.dart';

import 'components/assign_body.dart';

class AssignPage extends StatefulWidget {
  AssignPage({Key key}) : super(key: key);

  @override
  _AssignPageState createState() => _AssignPageState();
}

class _AssignPageState extends State<AssignPage> {
  void doLogout() async {
    await LoginService().doLogout();
    Navigator.popAndPushNamed(context, Paths.login);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        centerTitle: false,
        actions: [
          IconButton(
            icon: Icon(Icons.logout),
            onPressed: doLogout,
          )
        ],
      ),
      backgroundColor: Style.primaryColor,
      body: AssignBody(),
    );
  }
}
