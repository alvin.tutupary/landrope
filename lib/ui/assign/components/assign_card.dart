import 'dart:io';

import 'package:landrope/helper/constant.dart';
import 'package:landrope/helper/global.dart';
import 'package:landrope/helper/style.dart';
import 'package:landrope/model/assigns.dart';
import 'package:landrope/service/mobile_service.dart';
import 'package:landrope/shared/popups.dart';
import 'package:landrope/ui/assign/components/card_text.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';

class AssignCard extends StatelessWidget {
  final AssignRows data;
  const AssignCard({Key key, this.data}) : super(key: key);

  void loadAssignData(BuildContext context) async {
    String errMsg = "";
    try {
      if (data != null) {
        PopUps.confirmDialog(
          context: context,
          body: new Container(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Text(
                  "Dengan mengklik Setuju maka penugasan berikut ini akan mulai dilaksanakan dan diperhitungkan waktu penyelesaiannya",
                ),
                CardText(
                  label: "PROJECT",
                  value: data.project ?? "-",
                ),
                CardText(
                  label: "NO",
                  value: data.identifier ?? "-",
                ),
                CardText(
                  label: "PROSES",
                  value: globalData.strStep(data.step),
                ),
                CardText(
                  label: "CATEGORY",
                  value: globalData.strCategories(data.category),
                ),
                // CardText(label: "PTSK", value: data.ptsk)
              ],
            ),
          ),
          function1: () async {
            await MobileService().postTugasStep(data.key, data.routekey);
            Navigator.popAndPushNamed(context, Paths.assign);
          },
          labelButton1: "Setuju",
          function2: () => Navigator.pop(context),
          labelButton2: "Batal",
        );
      }
    } catch (e) {
      errMsg = e is DioError
          ? e.response?.statusCode == 400
              ? "Bad Request"
              : e.response?.statusCode == 401
                  ? "Unauthorized"
                  : e.error is SocketException
                      ? 'Cannot connect to server'
                      : 'Login Failed'
          : "Something Wrong!\n Please contact IT";
      PopUps.infoDialog(
        context: context,
        body: Container(
          child: Text(errMsg),
        ),
        function: () => errMsg == "Unauthorized"
            ? Navigator.popAndPushNamed(context, Paths.login)
            : Navigator.of(context).pop(),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    // Size size = MediaQuery.of(context).size;
    return Container(
      margin: EdgeInsets.symmetric(
        horizontal: Style.defaultPadding,
        vertical: Style.defaultPadding / 2,
      ),
      child: Stack(
        alignment: Alignment.bottomCenter,
        children: <Widget>[
          Container(
            height: 136,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(22),
              boxShadow: [Style.defaultShadow],
            ),
            child: Container(
                margin: EdgeInsets.only(right: 10),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(15),
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Row(
                      children: [
                        Padding(
                          padding: EdgeInsets.all(10),
                          child: Container(
                              width: Style.defaultPadding * 3.5,
                              height: Style.defaultPadding * 3.5,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(20),
                                  border: Border.all(
                                      color: Style.primaryColor, width: 3)),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Text(
                                    "${data.duration}",
                                    style: Style.titleTextStyle
                                        .copyWith(color: Style.primaryColor),
                                  ),
                                  Text(
                                    "Hari",
                                    style: Style.baseTextStyle
                                        .copyWith(color: Style.primaryColor),
                                  ),
                                ],
                              )),
                        ),
                        Container(
                            child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            InkWell(
                              onTap: () {
                                Navigator.pushNamed(
                                  context,
                                  Paths.assign_detail,
                                  arguments: data,
                                );
                              },
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    "${data.project}",
                                    style: Style.headerTextStyle
                                        .copyWith(color: Style.primaryColor),
                                  ),
                                  Text(
                                    "${data.identifier}",
                                    style: Style.subHeaderTextStyle
                                        .copyWith(color: Style.primaryColor),
                                  ),
                                  Text(
                                    "${globalData.strStep(data.step)}",
                                    style: Style.commonTextStyle
                                        .copyWith(color: Colors.black),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        )),
                      ],
                    ),
                    Container(
                        child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        data.accepted == null
                            ? IconButton(
                                icon: Icon(
                                  Icons.play_circle_outline,
                                  color: Style.greenColor,
                                ),
                                iconSize: 50,
                                color: Style.greenColor,
                                onPressed: () => loadAssignData(
                                      context,
                                    ))
                            : Container(),
                        Text(
                          "${globalData.strCategories(data.category)}",
                          style: Style.baseTextStyle
                              .copyWith(color: Style.primaryColor),
                        ),
                      ],
                    )),
                  ],
                )),
          ),
        ],
      ),
    );
  }
}
