import 'dart:io';

import 'package:landrope/helper/constant.dart';
import 'package:landrope/helper/style.dart';
import 'package:landrope/model/assigns.dart';
import 'package:landrope/service/assign_service.dart';
import 'package:landrope/shared/popups.dart';
import 'package:landrope/shared/search_box.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';

import 'category_list.dart';
import 'assign_card.dart';

class AssignBody extends StatefulWidget {
  AssignBody({Key key}) : super(key: key);

  @override
  _AssignBodyState createState() => _AssignBodyState();
}

class _AssignBodyState extends State<AssignBody> {
  List<AssignRows> _datas;
  bool isLoading = false;

  void initState() {
    _datas = new List<AssignRows>();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) async {
      loadAssignsData(context);
    });
    super.initState();
  }

  void loadAssignsData(BuildContext context) async {
    String errMsg = '';
    try {
      setState(() {
        isLoading = true;
      });
      Assigns res = await AssignService().getAssignLisit();
      setState(() {
        isLoading = false;
      });
      setState(() {
        _datas = res.rows;
      });
    } catch (e) {
      errMsg = e is DioError
          ? e.response?.statusCode == 400
              ? "Bad Request"
              : e.response?.statusCode == 401
                  ? "Unauthorized"
                  : e.error is SocketException
                      ? 'Cannot connect to server'
                      : 'Login Failed'
          : "Something Wrong!\n Please contact IT";
      PopUps.infoDialog(
        context: context,
        body: Container(
          child: Text(errMsg),
        ),
        function: () => errMsg == "Unauthorized"
            ? Navigator.popAndPushNamed(context, Paths.login)
            : Navigator.of(context).pop(),
      );
      setState(() {
        isLoading = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      bottom: false,
      child: Column(
        // crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Column(
            children: [
              Text(
                "Penugasan",
                style: Style.titleTextStyle,
              ),
              Text("Landrop 2.0", style: Style.subTitleTextStyle),
            ],
          ),
          // SearchBox(onChanged: (value) {}),
          // CategoryList(
          //   categories: ["Girik", "HGB", "SHM", "SHP", "Hibah", "Unknown"],
          // ),
          SizedBox(height: Style.defaultPadding / 2),
          Expanded(
            child: Stack(
              children: <Widget>[
                Container(
                  // margin: EdgeInsets.only(top: 10),
                  decoration: BoxDecoration(
                    color: Style.bgColor,
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(30),
                      topRight: Radius.circular(30),
                    ),
                  ),
                ),
                isLoading
                    ? Center(
                        child: CircularProgressIndicator(),
                      )
                    : ListView.builder(
                        itemCount: _datas.length,
                        itemBuilder: (context, index) => AssignCard(
                          data: _datas[index],
                        ),
                      )
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class AssignCardStatic extends StatelessWidget {
  const AssignCardStatic({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // Size size = MediaQuery.of(context).size;
    return Container(
      margin: EdgeInsets.symmetric(
        horizontal: Style.defaultPadding,
        vertical: Style.defaultPadding / 2,
      ),
      child: Stack(
        alignment: Alignment.bottomCenter,
        children: <Widget>[
          Container(
            height: 136,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(22),
              boxShadow: [Style.defaultShadow],
            ),
            child: Container(
              margin: EdgeInsets.only(right: 10),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(15),
              ),
            ),
          ),
          Positioned(
            bottom: 0,
            left: 0,
            child: SizedBox(
                height: 136,
                child: Row(
                  children: [
                    Padding(
                      padding: EdgeInsets.all(10),
                      child: Container(
                          width: Style.defaultPadding * 3.5,
                          height: Style.defaultPadding * 3.5,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(20),
                              border: Border.all(
                                  color: Style.primaryColor, width: 3)),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(
                                "23",
                                style: Style.titleTextStyle
                                    .copyWith(color: Style.primaryColor),
                              ),
                              Text(
                                "Hari",
                                style: Style.baseTextStyle
                                    .copyWith(color: Style.primaryColor),
                              ),
                            ],
                          )),
                    ),
                    Container(
                        child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          "data.project",
                          style: Style.headerTextStyle
                              .copyWith(color: Style.primaryColor),
                        ),
                        Text(
                          "{data.identifier}",
                          style: Style.subHeaderTextStyle
                              .copyWith(color: Style.primaryColor),
                        ),
                        Text(
                          "Desa Kali Baru",
                          style: Style.baseTextStyle,
                        ),
                        Text(
                          "Peningkatan Hak",
                          style: Style.baseTextStyle,
                        ),
                      ],
                    )),
                    Container(
                        child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        IconButton(
                            icon: Icon(
                              Icons.play_circle_outline,
                              color: Style.greenColor,
                            ),
                            color: Style.primaryColor,
                            onPressed: () {}),
                        Text(
                          "category",
                          style: Style.baseTextStyle
                              .copyWith(color: Style.primaryColor),
                        ),
                      ],
                    )),
                  ],
                )),
          ),
        ],
      ),
    );
  }
}
