import 'package:landrope/helper/style.dart';
import 'package:flutter/material.dart';

class CategoryList extends StatefulWidget {
  final List
      categories; // = ["Girik", "HGB", "SHM", "SHP", "Hibah", "Unknown"];
  CategoryList({Key key, this.categories}) : super(key: key);
  @override
  _CategoryListState createState() => _CategoryListState();
}

class _CategoryListState extends State<CategoryList> {
  int selectedIndex = 0;
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: Style.defaultPadding / 2),
      height: 30,
      child: ListView.builder(
        scrollDirection: Axis.horizontal,
        itemCount: widget.categories.length,
        itemBuilder: (context, index) => GestureDetector(
          onTap: () {
            setState(() {
              selectedIndex = index;
            });
          },
          child: Container(
            alignment: Alignment.center,
            margin: EdgeInsets.only(
              left: Style.defaultPadding,
              right: index == widget.categories.length - 1
                  ? Style.defaultPadding
                  : 0,
            ),
            padding: EdgeInsets.symmetric(horizontal: Style.defaultPadding),
            decoration: BoxDecoration(
              color: index == selectedIndex
                  ? Colors.white.withOpacity(0.4)
                  : Colors.transparent,
              borderRadius: BorderRadius.circular(6),
            ),
            child: Text(
              widget.categories[index],
              style: TextStyle(color: Colors.white),
            ),
          ),
        ),
      ),
    );
  }
}
