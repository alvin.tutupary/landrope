import 'package:landrope/helper/style.dart';
import 'package:flutter/material.dart';

class CardText extends StatelessWidget {
  final String label, value;
  const CardText({Key key, this.label, this.value}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      constraints:
          new BoxConstraints(maxWidth: MediaQuery.of(context).size.width - 84),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Text(label,
              style: Style.titleTextStyle
                  .copyWith(color: Colors.black, fontSize: 15)),
          Text(value, style: Style.baseTextStyle)
        ],
      ),
    );
  }
}
