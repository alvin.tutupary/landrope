import 'dart:io';

import 'package:landrope/helper/constant.dart';
import 'package:landrope/helper/routes.dart';
import 'package:landrope/helper/style.dart';
import 'package:landrope/service/login_service.dart';
import 'package:landrope/service/mobile_service.dart';
import 'package:landrope/shared/popups.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';

class AssignDetailSubmitPage extends StatefulWidget {
  final AssignDetailSubmitArgs data;
  AssignDetailSubmitPage({Key key, this.data}) : super(key: key);

  @override
  _AssignDetailSubmitPageState createState() => _AssignDetailSubmitPageState();
}

class _AssignDetailSubmitPageState extends State<AssignDetailSubmitPage> {
  final textPropVal = new GlobalKey<FormState>();
  int mkey;
  String propVal;

  @override
  void initState() {
    propVal = "";
    mkey = 0;
    super.initState();
  }

  void doSubmit() async {
    String errMsg = '';
    // bool isLoading;
    try {
      final key = textPropVal.currentState;

      if (key.validate()) {
        key.save();
        await MobileService().postTugasDtlSubmit(
          widget.data.tugasKeyDt.keydt,
          widget.data.tugasKeyDt.propkey,
          propVal,
          widget.data.dataAssignDetailRows.key,
          widget.data.dataAssignDetailRows.routekey,
          widget.data.dataAssignRows.key,
        );
        setState(() {
          propVal = "";
          mkey = 0;
        });
        Navigator.popAndPushNamed(
          context,
          Paths.assign_detail,
          arguments: widget.data.dataAssignRows,
        );
      }
    } catch (e) {
      errMsg = e is DioError
          ? e.response?.statusCode == 400
              ? "Bad Request"
              : e.response?.statusCode == 401
                  ? "Unauthorized! \n username atau password tidak sesuaii"
                  : e.error is SocketException
                      ? 'Cannot connect to server'
                      : 'Login Failed'
          : "Something Wrong!\n Please contact IT";
      PopUps.infoDialog(
        context: context,
        body: Container(
          child: Text(errMsg),
        ),
        function: () => Navigator.of(context).pop(),
      );
    }
  }

  void doLogout() async {
    await LoginService().doLogout();
    Navigator.popAndPushNamed(context, Paths.login);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        actions: [
          IconButton(icon: Icon(Icons.logout), onPressed: () => doLogout)
        ],
      ),
      resizeToAvoidBottomPadding: false,
      backgroundColor: Style.primaryColor,
      body: Container(
        margin: EdgeInsets.symmetric(horizontal: 20, vertical: 250),
        decoration: BoxDecoration(
            color: Colors.white, borderRadius: BorderRadius.circular(25)),
        padding: EdgeInsets.symmetric(vertical: 50, horizontal: 20),
        child: Column(
          children: [
            Text("Submit"),
            Text("Hallo ${widget.data.tugasKeyDt.doctype}"),
            Form(
                key: textPropVal,
                child: Column(
                  children: [
                    TextFormField(
                      validator: (value) => value.isEmpty
                          ? '${widget.data.tugasKeyDt.prop} anda kosong'
                          : null,
                      onSaved: (value) => propVal = value,
                      decoration: InputDecoration(
                          contentPadding: EdgeInsets.symmetric(vertical: 1),
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10.0),
                          ),
                          hintText: widget.data.tugasKeyDt.prop),
                    ),
                    RaisedButton(
                      onPressed: doSubmit,
                      child: Text("Submit"),
                    )
                  ],
                ))
          ],
        ),
      ),
    );
  }
}
