import 'package:landrope/helper/constant.dart';
import 'package:landrope/helper/style.dart';
import 'package:landrope/model/assigns.dart';
import 'package:landrope/service/login_service.dart';
import 'package:landrope/ui/assign_detail/components/assign_detail_body.dart';
import 'package:flutter/material.dart';

class AssignDetailPage extends StatefulWidget {
  final AssignRows data;
  AssignDetailPage({Key key, this.data}) : super(key: key);

  @override
  _AssignDetailPageState createState() => _AssignDetailPageState();
}

class _AssignDetailPageState extends State<AssignDetailPage> {
  void doLogout() async {
    await LoginService().doLogout();
    Navigator.popAndPushNamed(context, Paths.login);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        centerTitle: false,
        actions: [
          IconButton(icon: Icon(Icons.logout), onPressed: () => doLogout)
        ],
      ),
      backgroundColor: Style.primaryColor,
      body: AssignDetailnBody(data: widget.data),
    );
  }
}
