import 'dart:io';

import 'package:landrope/helper/constant.dart';
import 'package:landrope/helper/routes.dart';
import 'package:landrope/helper/style.dart';
import 'package:landrope/model/assign_details.dart';
import 'package:landrope/model/assigns.dart';
import 'package:landrope/model/mobile_tugas_keydt.dart';
import 'package:landrope/service/mobile_service.dart';
import 'package:landrope/shared/popups.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';

class AssignDetailCard extends StatelessWidget {
  final AssignRows dataAssignRows;
  final AssignDetailRows dataAssignDetailRows;
  const AssignDetailCard(
      {Key key, this.dataAssignRows, this.dataAssignDetailRows})
      : super(key: key);

  void loadAssignData(BuildContext context) async {
    String errMsg = "";
    try {
      //  Navigator.push(context, route)
    } catch (e) {
      errMsg = e is DioError
          ? e.response?.statusCode == 400
              ? "Bad Request"
              : e.response?.statusCode == 401
                  ? "Unauthorized"
                  : e.error is SocketException
                      ? 'Cannot connect to server'
                      : 'Login Failed'
          : "Something Wrong!\n Please contact IT";
      PopUps.infoDialog(
        context: context,
        body: Container(
          child: Text(errMsg),
        ),
        function: () => errMsg == "Unauthorized"
            ? Navigator.popAndPushNamed(context, Paths.login)
            : Navigator.of(context).pop(),
      );
    }
  }

  void toSubmitPage(BuildContext context, AssignDetailRows data) async {
    String errMsg = "";
    try {
      MobileTugasKeydt res = await MobileService().postTugasKeydt();

      Navigator.pushNamed(context, Paths.assign_detail_submit,
          arguments: AssignDetailSubmitArgs(
            dataAssignDetailRows: dataAssignDetailRows,
            dataAssignRows: dataAssignRows,
            tugasKeyDt: res,
          ));
    } catch (e) {
      errMsg = e is DioError
          ? e.response?.statusCode == 400
              ? "Bad Request"
              : e.response?.statusCode == 401
                  ? "Unauthorized"
                  : e.error is SocketException
                      ? 'Cannot connect to server'
                      : 'Login Failed'
          : "Something Wrong!\n Please contact IT";
      PopUps.infoDialog(
        context: context,
        body: Container(
          child: Text(errMsg),
        ),
        function: () => errMsg == "Unauthorized"
            ? Navigator.popAndPushNamed(context, Paths.login)
            : Navigator.of(context).pop(),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    // Size size = MediaQuery.of(context).size;
    return Container(
      margin: EdgeInsets.symmetric(
        horizontal: Style.defaultPadding,
        vertical: Style.defaultPadding / 2,
      ),
      child: Container(
          // margin: EdgeInsets.symmetric(vertical: 1),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(15),
          ),
          padding: EdgeInsets.symmetric(horizontal: 20, vertical: 25),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    "${dataAssignDetailRows.idBidang}",
                    style: Style.headerTextStyle
                        .copyWith(color: Style.primaryColor),
                  ),
                  dataAssignDetailRows.state == 13
                      ? IconButton(
                          icon: Icon(
                            Icons.play_circle_outline,
                            color: Style.greenColor,
                          ),
                          color: Style.primaryColor,
                          onPressed: () =>
                              toSubmitPage(context, dataAssignDetailRows))
                      : Container(),
                ],
              ),
              Text(
                "${dataAssignDetailRows.namaSurat}",
                style: Style.subHeaderTextStyle
                    .copyWith(color: Style.primaryColor),
              ),
              Text(
                "${dataAssignDetailRows.luasSurat}/${dataAssignDetailRows.luasDibayar}",
                style: Style.baseTextStyle,
              ),
              Text(
                "${dataAssignDetailRows.alasHak}",
                style: Style.baseTextStyle,
              ),
            ],
          )),

      // child: Stack(
      //   alignment: Alignment.bottomCenter,
      //   children: <Widget>[
      //     Container(
      //       height: 136,
      //       decoration: BoxDecoration(
      //         borderRadius: BorderRadius.circular(22),
      //         boxShadow: [Style.defaultShadow],
      //       ),
      //       child: Container(
      //         margin: EdgeInsets.only(right: 10),
      //         decoration: BoxDecoration(
      //           color: Colors.white,
      //           borderRadius: BorderRadius.circular(15),
      //         ),
      //       ),
      //     ),
      //     Positioned(
      //       bottom: 0,
      //       left: 0,
      //       child: SizedBox(
      //           height: 136,
      //           child: Row(
      //             children: [
      //               Container(
      //                   padding: EdgeInsets.symmetric(horizontal: 20),
      //                   child: Column(
      //                     crossAxisAlignment: CrossAxisAlignment.start,
      //                     mainAxisAlignment: MainAxisAlignment.center,
      //                     children: [
      //                       Text(
      //                         "${dataAssignDetailRows.idBidang}",
      //                         style: Style.headerTextStyle
      //                             .copyWith(color: Style.primaryColor),
      //                       ),
      //                       Text(
      //                         "${dataAssignDetailRows.namaSurat}",
      //                         style: Style.subHeaderTextStyle
      //                             .copyWith(color: Style.primaryColor),
      //                       ),
      //                       Text(
      //                         "${dataAssignDetailRows.luasSurat}/${dataAssignDetailRows.luasDibayar}",
      //                         style: Style.baseTextStyle,
      //                       ),
      //                       Text(
      //                         "${dataAssignDetailRows.alasHak}",
      //                         style: Style.baseTextStyle,
      //                       ),
      //                     ],
      //                   )),
      //               Container(
      //                   child: Column(
      //                 mainAxisAlignment: MainAxisAlignment.center,
      //                 children: [
      //                   dataAssignDetailRows.state == 13
      //                       ? IconButton(
      //                           icon: Icon(
      //                             Icons.play_circle_outline,
      //                             color: Style.greenColor,
      //                           ),
      //                           color: Style.primaryColor,
      //                           onPressed: () =>
      //                               toSubmitPage(context, dataAssignDetailRows))
      //                       : Container(),
      //                   Text(
      //                     "${dataAssignDetailRows.state}",
      //                     style: Style.baseTextStyle
      //                         .copyWith(color: Style.primaryColor),
      //                   ),
      //                 ],
      //               )),
      //             ],
      //           )),
      //     ),
      //   ],
      // ),
    );
  }
}
