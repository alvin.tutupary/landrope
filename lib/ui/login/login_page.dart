import 'dart:io';

import 'package:landrope/helper/constant.dart';
import 'package:landrope/helper/style.dart';
import 'package:landrope/service/login_service.dart';
import 'package:landrope/service/mobile_service.dart';
import 'package:landrope/shared/popups.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';

class LoginPage extends StatefulWidget {
  LoginPage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final formKey = new GlobalKey<FormState>();
  String username;
  String password;

  @override
  void initState() {
    username = '';
    password = '';
    super.initState();
  }

  void doLogin() async {
    String errMsg = '';
    // bool isLoading;
    try {
      final key = formKey.currentState;

      if (key.validate()) {
        key.save();

        bool resUsrName = await MobileService().getMobileAllowed(username);
        if (resUsrName) {
          await LoginService().doLogin(username, password);
          setState(() {
            username = '';
            password = '';
          });
          Navigator.popAndPushNamed(context, Paths.assign);
        } else {
          PopUps.infoDialog(
            context: context,
            body: Container(
              child: Text("Username ini tidak memiliki hak akses"),
            ),
            function: () => Navigator.of(context).pop(),
          );
        }
      }
    } catch (e) {
      errMsg = e is DioError
          ? e.response?.statusCode == 400
              ? "Bad Request"
              : e.response?.statusCode == 401
                  ? "Unauthorized! \n username atau password tidak sesuaii"
                  : e.error is SocketException
                      ? 'Cannot connect to server'
                      : 'Login Failed'
          : "Something Wrong!\n Please contact IT";
      PopUps.infoDialog(
        context: context,
        body: Container(
          child: Text(errMsg),
        ),
        function: () => Navigator.of(context).pop(),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      backgroundColor: Style.primaryColor,
      body: Container(
        margin: EdgeInsets.symmetric(vertical: 250, horizontal: 20),
        padding: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
        decoration: BoxDecoration(
            color: Colors.white, borderRadius: BorderRadius.circular(20)),
        child: Center(
          child: Form(
            key: formKey,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Column(
                  children: [
                    Text("Penugasan",
                        style: Style.titleTextStyle
                            .copyWith(color: Style.primaryColor)),
                    Text("Landrop 2.0",
                        style: Style.subTitleTextStyle
                            .copyWith(color: Style.primaryColor)),
                  ],
                ),
                TextFormField(
                  validator: (value) =>
                      value.isEmpty ? 'Please insert username' : null,
                  onSaved: (value) => username = value,
                  decoration: InputDecoration(
                    contentPadding: EdgeInsets.symmetric(vertical: 1),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    prefixIcon: Icon(
                      Icons.person,
                    ),
                    hintText: 'Username',
                  ),
                ),
                TextFormField(
                  obscureText: true,
                  validator: (value) =>
                      value.isEmpty ? 'Please insert password' : null,
                  onSaved: (value) => password = value,
                  decoration: InputDecoration(
                      contentPadding: EdgeInsets.symmetric(vertical: 1),
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                      prefixIcon: Icon(
                        Icons.vpn_key,
                      ),
                      hintText: 'Password'),
                ),
                RaisedButton(
                  onPressed: doLogin,
                  child: Text(
                    "Login",
                    style: Style.headerTextStyle,
                  ),
                  color: Style.primaryColor,
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
