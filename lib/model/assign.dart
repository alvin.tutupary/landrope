class Assign {
  String key;
  String identifier;
  int step;
  int category;
  String issued;
  String accepted;
  String closed;
  String ptsk;
  String project;
  String desa;
  int duration;
  String duedate;
  String overdue;

  Assign(
      {this.key,
      this.identifier,
      this.step,
      this.category,
      this.issued,
      this.accepted,
      this.closed,
      this.ptsk,
      this.project,
      this.desa,
      this.duration,
      this.duedate,
      this.overdue});

  Assign.fromJson(Map<String, dynamic> json) {
    key = json['key'];
    identifier = json['identifier'];
    step = json['step'];
    category = json['category'];
    issued = json['issued'];
    accepted = json['accepted'];
    closed = json['closed'];
    ptsk = json['ptsk'];
    project = json['project'];
    desa = json['desa'];
    duration = json['duration'];
    duedate = json['duedate'];
    overdue = json['overdue'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['key'] = this.key;
    data['identifier'] = this.identifier;
    data['step'] = this.step;
    data['category'] = this.category;
    data['issued'] = this.issued;
    data['accepted'] = this.accepted;
    data['closed'] = this.closed;
    data['ptsk'] = this.ptsk;
    data['project'] = this.project;
    data['desa'] = this.desa;
    data['duration'] = this.duration;
    data['duedate'] = this.duedate;
    data['overdue'] = this.overdue;
    return data;
  }
}
