class AssignDetails {
  int total;
  int page;
  int records;
  List<AssignDetailRows> rows;
  String tm;

  AssignDetails({this.total, this.page, this.records, this.rows, this.tm});

  AssignDetails.fromJson(Map<String, dynamic> json) {
    total = json['total'];
    page = json['page'];
    records = json['records'];
    if (json['rows'] != null) {
      rows = new List<AssignDetailRows>();
      json['rows'].forEach((v) {
        rows.add(new AssignDetailRows.fromJson(v));
      });
    }
    tm = json['tm'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['total'] = this.total;
    data['page'] = this.page;
    data['records'] = this.records;
    if (this.rows != null) {
      data['rows'] = this.rows.map((v) => v.toJson()).toList();
    }
    data['tm'] = this.tm;
    return data;
  }
}

class AssignDetailRows {
  String routekey;
  int state;
  String status;
  String statustm;
  int verb;
  String todo;
  // List<int> cmds;
  String key;
  String keyPersil;
  String idBidang;
  String noPeta;
  int luasDibayar;
  int luasSurat;
  String alasHak;
  String namaSurat;
  String pemilik;
  String group;
  int tahap;
  int satuan;
  int jumlah;
  String submitted;
  String resaccepted;
  String archived;
  String validated;

  AssignDetailRows(
      {this.routekey,
      this.state,
      this.status,
      this.statustm,
      this.verb,
      this.todo,
      // this.cmds,
      this.key,
      this.keyPersil,
      this.idBidang,
      this.noPeta,
      this.luasDibayar,
      this.luasSurat,
      this.alasHak,
      this.namaSurat,
      this.pemilik,
      this.group,
      this.tahap,
      this.satuan,
      this.jumlah,
      this.submitted,
      this.resaccepted,
      this.archived,
      this.validated});

  AssignDetailRows.fromJson(Map<String, dynamic> json) {
    routekey = json['routekey'];
    state = json['state'];
    status = json['status'];
    statustm = json['statustm'];
    verb = json['verb'];
    todo = json['todo'];
    // cmds = json['cmds'].cast<int>();
    key = json['key'];
    keyPersil = json['keyPersil'];
    idBidang = json['IdBidang'];
    noPeta = json['noPeta'];
    luasDibayar = json['luasDibayar'];
    luasSurat = json['luasSurat'];
    alasHak = json['alasHak'];
    namaSurat = json['namaSurat'];
    pemilik = json['pemilik'];
    group = json['group'];
    tahap = json['tahap'];
    satuan = json['satuan'];
    jumlah = json['jumlah'];
    submitted = json['submitted'];
    resaccepted = json['resaccepted'];
    archived = json['archived'];
    validated = json['validated'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['routekey'] = this.routekey;
    data['state'] = this.state;
    data['status'] = this.status;
    data['statustm'] = this.statustm;
    data['verb'] = this.verb;
    data['todo'] = this.todo;
    // data['cmds'] = this.cmds;
    data['key'] = this.key;
    data['keyPersil'] = this.keyPersil;
    data['IdBidang'] = this.idBidang;
    data['noPeta'] = this.noPeta;
    data['luasDibayar'] = this.luasDibayar;
    data['luasSurat'] = this.luasSurat;
    data['alasHak'] = this.alasHak;
    data['namaSurat'] = this.namaSurat;
    data['pemilik'] = this.pemilik;
    data['group'] = this.group;
    data['tahap'] = this.tahap;
    data['satuan'] = this.satuan;
    data['jumlah'] = this.jumlah;
    data['submitted'] = this.submitted;
    data['resaccepted'] = this.resaccepted;
    data['archived'] = this.archived;
    data['validated'] = this.validated;
    return data;
  }
}
