class Assigns {
  int total;
  int page;
  int records;
  List<AssignRows> rows;
  int role;
  Tm tm;

  Assigns({this.total, this.page, this.records, this.rows, this.role, this.tm});

  Assigns.fromJson(Map<String, dynamic> json) {
    total = json['total'];
    page = json['page'];
    records = json['records'];
    if (json['rows'] != null) {
      rows = new List<AssignRows>();
      json['rows'].forEach((v) {
        rows.add(new AssignRows.fromJson(v));
      });
    }
    role = json['role'];
    tm = json['tm'] != null ? new Tm.fromJson(json['tm']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['total'] = this.total;
    data['page'] = this.page;
    data['records'] = this.records;
    if (this.rows != null) {
      data['rows'] = this.rows.map((v) => v.toJson()).toList();
    }
    data['role'] = this.role;
    if (this.tm != null) {
      data['tm'] = this.tm.toJson();
    }
    return data;
  }
}

class AssignRows {
  String routekey;
  int state;
  String status;
  String statustm;
  int verb;
  String todo;
  // List<int> cmds;
  bool isCreator;
  String key;
  String instkey;
  String invalid;
  String identifier;
  int type;
  int step;
  int category;
  String created;
  String creator;
  String issued;
  String issuer;
  String delegated;
  String accepted;
  int duration;
  String closed;
  String pic;
  String keyPIC;
  String notaris;
  String project;
  String ptsk;
  String penampung;
  String today;

  AssignRows(
      {this.routekey,
      this.state,
      this.status,
      this.statustm,
      this.verb,
      this.todo,
      // this.cmds,
      this.isCreator,
      this.key,
      this.instkey,
      this.invalid,
      this.identifier,
      this.type,
      this.step,
      this.category,
      this.created,
      this.creator,
      this.issued,
      this.issuer,
      this.delegated,
      this.accepted,
      this.duration,
      this.closed,
      this.pic,
      this.keyPIC,
      this.notaris,
      this.project,
      this.ptsk,
      this.penampung,
      this.today});

  AssignRows.fromJson(Map<String, dynamic> json) {
    routekey = json['routekey'];
    state = json['state'];
    status = json['status'];
    statustm = json['statustm'];
    verb = json['verb'];
    todo = json['todo'];
    // cmds = json['cmds'].cast<int>();
    isCreator = json['isCreator'];
    key = json['key'];
    instkey = json['instkey'];
    invalid = json['invalid'];
    identifier = json['identifier'];
    type = json['type'];
    step = json['step'];
    category = json['category'];
    created = json['created'];
    creator = json['creator'];
    issued = json['issued'];
    issuer = json['issuer'];
    delegated = json['delegated'];
    accepted = json['accepted'];
    duration = json['duration'];
    closed = json['closed'];
    pic = json['pic'];
    keyPIC = json['keyPIC'];
    notaris = json['notaris'];
    project = json['project'];
    ptsk = json['ptsk'];
    penampung = json['penampung'];
    today = json['today'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['routekey'] = this.routekey;
    data['state'] = this.state;
    data['status'] = this.status;
    data['statustm'] = this.statustm;
    data['verb'] = this.verb;
    data['todo'] = this.todo;
    // data['cmds'] = this.cmds;
    data['isCreator'] = this.isCreator;
    data['key'] = this.key;
    data['instkey'] = this.instkey;
    data['invalid'] = this.invalid;
    data['identifier'] = this.identifier;
    data['type'] = this.type;
    data['step'] = this.step;
    data['category'] = this.category;
    data['created'] = this.created;
    data['creator'] = this.creator;
    data['issued'] = this.issued;
    data['issuer'] = this.issuer;
    data['delegated'] = this.delegated;
    data['accepted'] = this.accepted;
    data['duration'] = this.duration;
    data['closed'] = this.closed;
    data['pic'] = this.pic;
    data['keyPIC'] = this.keyPIC;
    data['notaris'] = this.notaris;
    data['project'] = this.project;
    data['ptsk'] = this.ptsk;
    data['penampung'] = this.penampung;
    data['today'] = this.today;
    return data;
  }
}

class Tm {
  int ticks;
  int days;
  int hours;
  int milliseconds;
  int minutes;
  int seconds;
  double totalDays;
  double totalHours;
  double totalMilliseconds;
  double totalMinutes;
  double totalSeconds;

  Tm(
      {this.ticks,
      this.days,
      this.hours,
      this.milliseconds,
      this.minutes,
      this.seconds,
      this.totalDays,
      this.totalHours,
      this.totalMilliseconds,
      this.totalMinutes,
      this.totalSeconds});

  Tm.fromJson(Map<String, dynamic> json) {
    ticks = json['ticks'];
    days = json['days'];
    hours = json['hours'];
    milliseconds = json['milliseconds'];
    minutes = json['minutes'];
    seconds = json['seconds'];
    totalDays = json['totalDays'];
    totalHours = json['totalHours'];
    totalMilliseconds = json['totalMilliseconds'];
    totalMinutes = json['totalMinutes'];
    totalSeconds = json['totalSeconds'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['ticks'] = this.ticks;
    data['days'] = this.days;
    data['hours'] = this.hours;
    data['milliseconds'] = this.milliseconds;
    data['minutes'] = this.minutes;
    data['seconds'] = this.seconds;
    data['totalDays'] = this.totalDays;
    data['totalHours'] = this.totalHours;
    data['totalMilliseconds'] = this.totalMilliseconds;
    data['totalMinutes'] = this.totalMinutes;
    data['totalSeconds'] = this.totalSeconds;
    return data;
  }
}
