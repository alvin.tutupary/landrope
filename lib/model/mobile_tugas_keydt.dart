class MobileTugasKeydt {
  String doctype;
  String keydt;
  String prop;
  int propkey;

  MobileTugasKeydt({this.doctype, this.keydt, this.prop, this.propkey});

  MobileTugasKeydt.fromJson(Map<String, dynamic> json) {
    doctype = json['doctype'];
    keydt = json['keydt'];
    prop = json['prop'];
    propkey = json['propkey'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['doctype'] = this.doctype;
    data['keydt'] = this.keydt;
    data['prop'] = this.prop;
    data['propkey'] = this.propkey;
    return data;
  }
}
