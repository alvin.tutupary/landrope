import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

enum Actions { yes, no }

class PopUps {
  static Future infoDialog(
      {BuildContext context, Container body, Function function}) async {
    await showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context) {
          return Dialog(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0)),
              child: Container(
                  height: 180.0,
                  width: 150.0,
                  decoration:
                      BoxDecoration(borderRadius: BorderRadius.circular(20.0)),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Container(
                        height: 30.0,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(10.0),
                              topRight: Radius.circular(10.0),
                            ),
                            color: Theme.of(context).primaryColor),
                      ),
                      body,
                      FlatButton(
                          child: Center(
                            child: Text(
                              'OK',
                              style: TextStyle(
                                  fontSize: 14.0,
                                  color: Theme.of(context).primaryColor),
                            ),
                          ),
                          onPressed: () => function(),
                          color: Colors.transparent)
                    ],
                  )));
        });
  }

  static Future<Actions> confirmDialog(
      {BuildContext context,
      Container body,
      String labelButton1,
      String labelButton2,
      Function function1,
      Function function2}) async {
    final action = await showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context) {
          return Dialog(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0)),
              child: Container(
                  width: 150.0,
                  decoration:
                      BoxDecoration(borderRadius: BorderRadius.circular(20.0)),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Container(
                        height: 30.0,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(10.0),
                              topRight: Radius.circular(10.0),
                            ),
                            color: Theme.of(context).primaryColor),
                      ),
                      body,
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: <Widget>[
                          RaisedButton(
                              child: Center(
                                child: Text(
                                  labelButton1,
                                  style: TextStyle(
                                      fontSize: 14.0,
                                      color: Theme.of(context).primaryColor),
                                ),
                              ),
                              onPressed: () => function1()),
                          RaisedButton(
                            child: Center(
                              child: Text(
                                labelButton2,
                                style: TextStyle(
                                    fontSize: 14.0,
                                    color: Theme.of(context).primaryColor),
                              ),
                            ),
                            onPressed: () => function2(),
                          ),
                        ],
                      )
                    ],
                  )));
        });
    return action;
  }
}
